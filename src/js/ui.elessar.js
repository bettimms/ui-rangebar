/**
 * Created by betim on 2/14/2017.
 */

(function (app) {
    //#region Helpers
    //#endregion

    app.service("UIService", [UIService]);
    function UIService() {
        var service = {};
        service.setNgStyle = function (style, rangeBar) {
            // console.log(rangeBar);
            angular.element(".elessar-range").attr("ng-style", style);
        }
        service.setNgClass = function (className, el, rangeBar) {
            if (el) {
                angular.element(el).attr("ng-class", className);
            }
            else {
                angular.element(".elessar-range").attr("ng-class", className);
            }
        }
        return service;
    }

    app.directive("uiElessar", ["$parse", "$compile", "$timeout", "UIService", uiElessar]);
    function uiElessar($parse, $compile, $timeout, UIService) {
        return {
            restrict: "EA",
            require: "ngModel",
            scope: {
                ngModel: "=ngModel",
                uiInstance: "=?"//rangeBar instance
            },
            link: function (scope, element, attrs, ngModel) {

                var rangeChanged = false;

                //#region Attributes
                //TODO TBD whether to keep attributes or go by js options only
                var onFormat = attrParser(attrs.valueFormat);
                var onParse = attrParser(attrs.valueParse);
                var onLabel = attrParser(attrs.label);
                var onChange = attrParser(attrs.onChange);
                var onChanging = attrParser(attrs.onChanging);
                var snap = attrParser(attrs.snap);
                var readonly = attrParser(attrs.readonly);
                var min = attrParser(attrs.min);
                var max = attrParser(attrs.max);
                var minSize = attrParser(attrs.minSize);
                var allowDelete = attrParser(attrs.allowDelete);
                var vertical = attrParser(attrs.vertical);
                var htmlLabel = attrParser(attrs.htmlLabel);
                var allowSwap = attrParser(attrs.allowSwap);
                var values = attrParser(attrs.values);
                var indicator = attrParser(attrs.indicator);
                var bgLabels = attrParser(attrs.bgLabels);

                var uiOptions = attrParser(attrs.uiOptions);
                var defaultOptions = {
                    values: values || [], // array of value pairs; each pair is the min and max of the range it creates
                    readonly: readonly || false, // whether this bar is read-only
                    min: min || 0, // value at start of bar
                    max: max || 100, // value at end of bar
                    valueFormat: onFormat || function (val) {
                        return val;
                    }, // formats a value on the bar for output
                    valueParse: onParse || function (val) {
                        return val;
                    },
                    label: onLabel || function (values) {
                        return values;
                    },
                    // parses an output value for the bar
                    snap: snap || 0, // clamps range ends to multiples of this value (in bar units)
                    minSize: minSize || 0, // smallest allowed range (in bar units)
                    maxRanges: Infinity, // maximum number of ranges allowed on the bar
                    bgMarks: {
                        count: 0, // number of value labels to write in the background of the bar
                        interval: Infinity, // provide instead of count to specify the space between labels
                        label: "" // string or function to write as the text of a label. functions are called with normalised values.
                    },
                    indicator: indicator, // pass a function(RangeBar, Indicator, Function?) Value to calculate
                    // where to put
                    // a current indicator, calling the function whenever you want the position to be recalculated
                    allowDelete: allowDelete || true, // set to true to enable double-middle-click-to-delete
                    deleteTimeout: 3000, // maximum time in ms between middle clicks
                    vertical: vertical || false, // if true the rangebar is aligned vertically, and given the class
                    // elessar-vertical
                    bounds: null, // a function that provides an upper or lower bound when a range is being dragged. call with the range that is being moved, should return an object with an upper or lower key
                    htmlLabel: htmlLabel || false, // if true, range labels are written as html
                    allowSwap: allowSwap || true, // swap ranges when dragging past,
                    bgLabels: bgLabels || 4,
                    onAddRange: function (range) {
                    },
                    onRangeClick: function (evt) {
                    }
                };
                var options = angular.extend({}, defaultOptions, uiOptions);
                //#endregion

                var rangeBar = new RangeBar(options);
                element.append(rangeBar.$el);
                scope.uiInstance = rangeBar;//expose instance

                UIService.setNgStyle(options.rangeStyle, rangeBar);
                // UIService.setNgClass(options.rangeClass, rangeBar);//It's by default
                $compile(element.contents())(scope);
                //In case user sets default values update ng-model

                //(Either values of ngModel, this is at the begging only)If values are set then use values instead of
                // ngModel
                if (options.values.length > 0) setNgModel(options.values)
                else setRangesFromModel(scope.ngModel);

                var timeout = null;
                rangeBar.$el.on("changing", function (ev, ranges, bar) {
                    setNgModel(ranges);
                    if (onChanging) onChanging(ev, ranges, bar);
                    if (options.onChanging) options.onChanging(ev, ranges, changed);

                }).on("change", function (ev, ranges, changed) {
                    //Fix for multiple calls
                    //https://github.com/quarterto/Elessar/issues/99
                    $timeout.cancel(timeout);
                    timeout = $timeout(function () {
                        changeFn();
                        rangeChanged = false;
                    }, 0);
                    function changeFn() {
                        var newRange = angular.element(changed).data("element") || changed;
                        if (onChange) onChange(ev, ranges, changed);
                        if (options.onChange) options.onChange(ev, ranges, changed);
                        // $compile(element.contents())(scope);
                    }
                });
                rangeBar.$el.on("addrange", function (ev, range, bar) {
                    var ranges = rangeBar.val();
                    var index = rangeBar.findGap(range);
                    setNgModel(ranges);
                    rangeBar.options.onAddRange(ev, range, bar, index);
                    rangeChanged = false;

                    addEventListener("click", [bar], function (ev) {
                        if (!rangeChanged) {
                            rangeBar.options.onClick(ev);
                        }
                        rangeChanged = false;
                    });
                });

                //Add event for predefined bars
                addEventListener("click", rangeBar.ranges, function (ev) {
                    if (!rangeChanged) {
                        rangeBar.options.onClick(ev);
                    }
                    rangeChanged = false;
                });
                function setNgModel(ranges) {
                    if (!ranges) return;
                    scope.$applyAsync(function () {
                        scope.ngModel = ranges;
                    });
                    rangeChanged = true;
                }

                //TODO optimize for all who uses it (assign from a single place and refresh them)
                function addEventListener(eventName, elements, callback) {
                    rangeChanged = false;
                    elements.forEach(function (el) {
                        el.on(eventName, callback);
                    })
                }

                function setRangesFromModel(ranges) {
                    ranges.forEach(function (range) {
                        var a = rangeBar.abnormalise(range[0]);
                        var b = rangeBar.abnormalise(range[1]);
                        rangeBar.addRange([a, b]);
                    });
                }

                // scope.$watchCollection("ngModel", function (modelValue) {
                //     console.log(modelValue);
                // });

                function attrParser(attrName) {
                    var fn, controllerScope = Object.prototype.toString.call($parse(attrName)(scope)),
                        controllerAsScope = Object.prototype.toString.call($parse(attrName)(scope.$parent));

                    if ((controllerScope === "[object Function]") || (controllerScope === "[object Object]")) {
                        fn = $parse(attrName)(scope);
                    } else if ((controllerAsScope === "[object Function]") || (controllerAsScope === "[object" +
                        " Object]")) {
                        fn = $parse(attrName)(scope.$parent)
                    }
                    return fn;
                }
            }
        }
    }
})(angular.module("ui.elessar", []));