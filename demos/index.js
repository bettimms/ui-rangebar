/**
 * Created by betim on 2/15/2017.
 */
function getRandomColor() {
    return '#' + (0x1000000 + (Math.random()) * 0xffffff).toString(16).substr(1, 6);
}
(function (app) {

    app.controller("HomeController", ["$scope", "$http", HomeController])
    function HomeController($scope, $http) {
        var vm = this;
        var values = "";
        vm.rangeVal = "20";

        vm.message = "WORKS";
        vm.snap = 5;
        vm.values = [];

        vm.options = {
            rangeStyle: "{color:'white','background-color':'green','text-align':'center'}",
            // rangeClass: "red",
            values: [[1, 5], [20, 30], [50, 60]],
            onChange: function (ev, ranges, changed) {
                // console.log(changed, ranges);
            },
            onAddRange: function (evt, range, bar, index) {
                bar.$el.css("background-color", getRandomColor());
            },
            onClick: function (ev) {
                console.log(ev);
            }
        };
    }
})(angular.module("app", ["ui.elessar"]))